$(function(){
    //MODAIS
    $("#btn_add_course").click(function(){
        clearErrors();
        $("#form_course")[0].reset();
        $("#servico_img_path").attr("src","");
        $("#modal_course").modal();
    }),
    $("#btn_add_team").click(function(){
        $("#form_profissional")[0].reset();
        $("#prof_img_path").attr("src","");
        $("#modal_team").modal();
    }),
    $("#btn_add_user").click(function(){
        $("#form_cliente")[0].reset();
        $("#modal_user").modal();
    });

   //IMPORTANDO IMAGENS

   
   $("#btn_upload_servico_img").change(function(){
    uploadImg($(this),$("#servico_img_path"),$("#course_img"));
   });
   $("#btn_upload_prof_img").change(function(){
    uploadImg($(this),$("#prof_img_path"),$("#team_img"));
   });

   //SALVANDO FORMULARIO SERVICOS
   $("#form_course").submit(function(){

    $.ajax({
         type: "POST",
         url: BASE_URL + "restrict/ajax_save_service",
         dataType: "json",
         data: $(this).serialize(),
         beforeSend: function(){
            clearErrors();
            $("#btn_save_course").siblings(".help-block").html(loadingImg("Verificando...")); 
         },
         success: function(response){
             clearErrors();
             if(response["status"]){
                $("#modal_course").modal("hide");
                location.reload();
             }else{
                showErrorsModal(response["error_list"]);
             }
         } 
    })


        return false;
   });


   //SALVANDO FORMULARIO PROFISSIONAIS

   $("#form_profissional").submit(function(){

    $.ajax({
         type: "POST",
         url: BASE_URL + "restrict/ajax_save_prof",
         dataType: "json",
         data: $(this).serialize(),
         beforeSend: function(){
            clearErrors();
            $("#btn_save_team").siblings(".help-block").html(loadingImg("Verificando..."));
         },         
         success: function(response){
             clearErrors();
             console.log(response);
             if(response["status"]){
                $("#modal_team").modal("hide");
                location.reload();
             }else{
                showErrorsModal(response["error_list"]);
             }
         } 
    })


        return false;
   });

   //SALVANDO FORMULARIO PROFISSIONAIS

   $("#form_cliente").submit(function(){

    $.ajax({
         type: "POST",
         url: BASE_URL + "restrict/ajax_save_cliente",
         dataType: "json",
         data: $(this).serialize(),
         beforeSend: function(){
            clearErrors();
            $("#btn_save_user").siblings(".help-block").html(loadingImg("Verificando..."));
         },         
         success: function(response){
             clearErrors();
             console.log(response);
             if(response["status"]){
                $("#modal_user").modal("hide");
                
             }else{
                showErrorsModal(response["error_list"]);
             }
         } 
    })


        return false;
   });
   function active_btn_servico(){
      $(".btn-edit-servico").click(function() {

         $.ajax({
            type: "POST",
            url: BASE_URL + "restrict/ajax_get_servico_data",
            dataType: "json",
            data: {"id_servico": $(this).attr("id_servico")},
            success: function(response) {
               clearErrors();
               $("#form_course")[0].reset();
               $.each(response["input"], function(id, value) {
                  $("#"+id).val(value);
               });
               $("#servico_img_path").attr("src", response["img"] ["servico_img_path"]);

               $("#modal_course").modal();
            }
         })

      });
      $(".btn-del-servico").click(function() {

         servico_id = $(this);
       
         
               $.ajax({
               type: "POST",
               url: BASE_URL + "restrict/ajax_del_servico_data",
               dataType: "json",
               data: {"id_servico": servico_id.attr("id_servico")},
               success: function() {   
                  location.reload();  
              }
               })
         
      });
   }
      

  

   
   

   var dt_couses = $('#dt_couses').DataTable({
      "autoWidth":false,
      "serverSide":true,
      "ajax":{
         "url" : BASE_URL + "restrict/ajax_list_servico",
         "type":"POST",
      },
      "columnDefs":[
         { targets: "no-sort", ordeable:false},
         { targets:"dt-center",className:"dt-center"},
      ],
      "initComplete": function(){
         active_btn_servico();
      }
   });



   function active_btn_prof(){
      $(".btn-edit-profissional").click(function() {

         $.ajax({
            type: "POST",
            url: BASE_URL + "restrict/ajax_get_prof_data",
            dataType: "json",
            data: {"id_profissional": $(this).attr("id_profissional")},
            success: function(response) {
               clearErrors();
               $("#form_profissional")[0].reset();
               $.each(response["input"], function(id, value) {
                  $("#"+id).val(value);
               });
               $("#prof_img_path").attr("src", response["img"] ["prof_img_path"]);

               $("#modal_team").modal();
            }
         })

      });
      $(".btn-del-profissional").click(function() {

         id_profissional = $(this);
       
         
               $.ajax({
               type: "POST",
               url: BASE_URL + "restrict/ajax_del_prof_data",
               dataType: "json",
               data: {"id_profissional": id_profissional.attr("id_profissional")},
               success: function() {   
                  location.reload();  
              }
               })
         
      });
   }


   
   var dt_team = $('#dt_team').DataTable({
      "autoWidth":false,
      "serverSide":true,
      "ajax":{
         "url" : BASE_URL + "restrict/ajax_list_profissional",
         "type":"POST",
      },
      "columnDefs":[
         { targets: "no-sort", ordeable:false},
         { targets:"dt-center",className:"dt-center"},
      ],
     
      "initComplete": function(){
         active_btn_prof();
      }
   });


   function active_btn_cli(){
      $(".btn-edit-cliente").click(function() {

         $.ajax({
            type: "POST",
            url: BASE_URL + "restrict/ajax_get_cli_data",
            dataType: "json",
            data: {"id_cliente": $(this).attr("id_cliente")},
            success: function(response) {
               clearErrors();
               $("#form_cliente")[0].reset();
               $.each(response["input"], function(id, value) {
                  $("#"+id).val(value);
               });

               $("#modal_user").modal();
            }
         })

      });
      $(".btn-del-cliente").click(function() {

         id_cliente = $(this);
       
         
               $.ajax({
               type: "POST",
               url: BASE_URL + "restrict/ajax_del_cli_data",
               dataType: "json",
               data: {"id_cliente": id_cliente.attr("id_cliente")},
               success: function() {   
                  location.reload();  
              }
               })
         
      });
   }

   var dt_user = $('#dt_user').DataTable({
      "autoWidth":false,
      "serverSide":true,
      "ajax":{
         "url" : BASE_URL + "restrict/ajax_list_cliente",
         "type":"POST",
      },
      "columnDefs":[
         { targets: "no-sort", ordeable:false},
         { targets:"dt-center",className:"dt-center"},
      ],
      "initComplete": function(){
         active_btn_cli();
      }
   });

  


   



  

  
  


})