<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sobre extends CI_Controller {

	public function index()
	{
		$this->load->model("profissionais_model");
		$_SESSION['prof'] = $this->profissionais_model->show_profs();
		$profissionais = $this->profissionais_model->show_profs();
	
		
		$data = array(
			
			"profissionais" => $profissionais,
			
		);
		$this->template->show('sobre');
	
	}
}