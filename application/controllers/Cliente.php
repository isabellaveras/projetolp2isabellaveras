<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library("session");
	}

	public function index()
	{
		$this->template->show('cliente');
		
	
	}

	//FAZER LOGOOF
	public function logoff(){
		$this->session->sess_destroy();
		
	}
}