<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Restrict extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library("session");
	}

	public function index()
	{
		if($this->session->userdata("login")){
			$data = array(
				"styles"=>array(
					"dataTables.bootstrap.min.css",
					"datatables.min.css",
				),
				"scripts"=>array(
					"sweetalert2.all.min.js",
					"dataTables.bootstrap.min.js",
					"datatables.min.js",				
					"restrict.js",
					"util.js"
				),
			);
			$this->template->show('restrict.php',$data);

			
		}else{

			$data = array(
				"scripts"=>array(
					"util.js",
					"login.js"
				)
			);
			$this->template->show('login.php',$data);
		}
	}

	//FAZER LOGOOF
	public function logoff(){
		$this->session->sess_destroy();
		header("Location: " . base_url() . "restrict");
	}

	//FAZER LOGIN
	public function ajax_login(){

		if(!$this->input->is_ajax_request()){
			exit("Acesso direto não permitido");
		}

		$json = array();
		$json["status"] = 1;
		$json["error_list"] = array();

		$username = $this->input->post("username");
		$password = $this->input->post("password");
		$tp_login = $this->input->post("tp_login");
		
		
		if(empty($username)){
			$json["status"] = 0;
			$json["error_list"]["#username"] = "Usuário não pode ser vazio!";
		}else{
			$this->load->model("users_model");
			$result = $this->users_model->get_user_data($username);
			if($result){
				$login = $result->login;
				$password_hash = $result->password_hash;
				$tp_login = $result->tp_login;

				if(password_verify($password, $password_hash)){
					$this->session->set_userdata("login",$login);
					$this->session->set_userdata("tp_login",$tp_login);
					$json["tp_login"] = $tp_login;
				}else{
					$json["status"] = 0;
				}
				
				
				
			}
			else{
					$json["status"] = 0;
			}
			if($json["status"] == 0){
					$json["error_list"]["#btn_login"] = "Usuário e/ou senha incorretos!";
			}			
		}
		echo json_encode($json);
	}


//IMPOTACAO DE IMAGENS
public function ajax_import_image(){
 
	if(!$this->input->is_ajax_request()){
		exit("Acesso direto não permitido");
	}

	$config["upload_path"] = "./tmp/";
	$config["allowed_types"] = "gif|png|jpg";
	$config["overwrite"] = TRUE;

	$this->load->library("upload", $config);

	$json = array();
	$json["status"] = 1;

	//testar upload

	if(!$this->upload->do_upload("image_file")){
		$json["status"] = 0;
		$json["error"] = $this->upload->display_errors("","");
	}
	else{
		if($this->upload->data()["file_size"] <= 1024){
			$file_name = $this->upload->data()["file_name"];
			$json["img_path"] = base_url()."tmp/".$file_name;

		}else{
			$json["status"] = 0;
			$json["error"] = "Arquivo não deve ser maior que 1MB!";
		}

	}
	echo json_encode($json);
}

	

	//SALVAR E VALIDAR FORMULARIO SERVIÇOS

	public function ajax_save_service(){
 
		if(!$this->input->is_ajax_request()){
			exit("Acesso direto não permitido");
		}
		
		$json = array();
		$json["status"] = 0;
		$json["error_list"] = array();

		$this->load->model("servicos_model");

		$data = $this->input->post();

		$data = $this->input->post();
		
		//VALIDAR NOME

		if(empty($data["nm_servico"])){
			$json["error_list"]["#nm_servico"] = "O ser é um campo obrigatório";
		} 
		else{
		  if($this->servicos_model->is_duplicate("nm_servico", $data["nm_servico"]. $data["id_servico"])){
			  $json["error_list"]["#nm_servico"] = "Nome de servico já existe";
		  }
			
		}

		//VALIDAR PRECO

		$data["preco"] = floatval($data["preco"]);
		if(empty($data["preco"])){
			$json["error_list"]["#preco"] = "O preço é um campo obrigatório";
		} 

		//VALIDAR IMAGEM

		if(!empty($json["error_list"])){
			$json["status"] = 0;
		} 
		else{
		  if(!empty($data["course_img"])){
			  $file_name = basename($data["course_img"]);
			  $old_path = getcwd() . "/tmp/" . $file_name;
			  $new_path = getcwd() . "/assets/img/servicos/" . $file_name;
			  rename($old_path, $new_path);

			  $data["course_img"] = "/assets/img/servicos/" . $file_name;
		  }else{
			unset($data["course_img"]);
		  }
			//VALIDAR ID

			if(empty($data["id_servico"])){
				$this->servicos_model->insert($data);
			}
			else{
				$id_servico = $data["id_servico"];
				unset($data["id_servico"]);
				$this->servicos_model->update($id_servico, $data);
			}

		}

		echo json_encode($json);

	}




	public function ajax_save_prof(){
			
			if(!$this->input->is_ajax_request()){
				exit("Acesso direto não permitido");
			}
			
			$json = array();
			$json["status"] = 0;
			$json["error_list"] = array();
	
			$this->load->model("profissionais_model");
	
			$data = $this->input->post();
		
		
			
			//VALIDAR NOME
	
			if(empty($data["nome"])){
				$json["error_list"]["#nome"] = "O ser é um campo obrigatório";
			} 
			//VALIDAR CPF
	
			if(empty($data["cpf_profissional"])){
				$json["error_list"]["#cpf_profissional"] = "O ser é um campo obrigatório";		  
			}else{
					if($this->profissionais_model->is_duplicate("cpf_profissional", $data["cpf_profissional"]. $data["id_profissional"])){
						$json["error_list"]["#cpf_profissional"] = "CPF ja cadastrado já existe";
					}
					  
				  }
			//VALIDAR CELULAR
	
			if(empty($data["celular"])){
				$json["error_list"]["#celular"] = "O ser é um campo obrigatório";
			} 
			//VALIDAR TELEFONE
	
			if(empty($data["telefone"])){
				$json["error_list"]["#telefone"] = "O ser é um campo obrigatório";
			} 
			//VALIDAR ENDERECO
	
			if(empty($data["endereco"])){
				$json["error_list"]["#endereco"] = "O ser é um campo obrigatório";
			}			
	
			//VALIDAR EMAIL
	
			if(empty($data["email_profissional"])){
				$json["error_list"]["#email_profissional"] = "O ser é um campo obrigatório";
			} 
			else{
			  if($this->profissionais_model->is_duplicate("email_profissional", $data["email_profissional"]. $data["id_profissional"])){
				  $json["error_list"]["#email_profissional"] = "email já existe";
			  }
				if($this->profissionais_model->is_duplicate("email_profissional", $data["email_profissional"]. $data["id_profissional"])){
					$json["error_list"]["#email_profissional"] = "Email ja cadastrado já existe";			
				  
				}else{
					if($data["email_profissional"]!=$data["conf_email"]){
						$json["error_list"]["#email_profissional"] = "";
						$json["error_list"]["#conf_email"] = "Emials divergentes";
					}
				}
				
				
			}	
	
			//VALIDAR SENHA
			if(empty($data["password"])){
				$json["error_list"]["#password"] = "O ser é um campo obrigatório";
			} 
			else{
				if($data["password"]!=$data["password_confirm"]){
					$json["error_list"]["#password"] = "";
					$json["error_list"]["#password_confirm"] = "Senhas divergentes";
				}
			  }
				
			
			//VALIDAR IMAGEM
	
			if(!empty($json["error_list"])){
				$json["status"] = 0;
			} 
			else{
			  if(!empty($data["team_img"])){
				  $file_name = basename($data["team_img"]);
				  $old_path = getcwd() . "/tmp/" . $file_name;
				  $new_path = getcwd() . "/assets/img/profissionais/" . $file_name;
				  rename($old_path, $new_path);
	
				  $data["team_img"] = "/assets/img/profissionais/" . $file_name;
			  }
			  else{
				unset($data["team_img"]);
			  }
	
			  
			  //$data["password_hash"] = password_hash($data["password"].PASSWORD_DEFAULT);
			  
			  unset($data["password"]);
			  unset($data["password_confirm"]);
			  unset($data["conf_email"]);
			  unset($data["prof_realiza"]);
			  unset($data["login"]);
				//VALIDAR ID
	
				if(empty($data["id_profissional"])){
					
					unset($data["id_profissional"]);
					$this->profissionais_model->insert($data);
					

				}
				else{
					$id_profissional = $data["id_profissional"];
					unset($data["id_profissional"]);
					$this->profissionais_model->update($id_profissional, $data);
				}
	
			}
			
			echo json_encode($json);
	
		}





		public function ajax_save_cliente(){
			
			if(!$this->input->is_ajax_request()){
				exit("Acesso direto não permitido");
			}
			
			$json = array();
			$json["status"] = 0;
			$json["error_list"] = array();
	
			$this->load->model("users_model");
	
			$data = $this->input->post();
		
		
			
			//VALIDAR NOME
	
			if(empty($data["nome"])){
				$json["error_list"]["#nome_cli"] = "O ser é um campo obrigatório";
			} 
			//VALIDAR CPF
	
			if(empty($data["cliente_cpf"])){
				$json["error_list"]["#cliente_cpf"] = "O ser é um campo obrigatório";
			} 
			//VALIDAR CELULAR
	
			if(empty($data["celular"])){
				$json["error_list"]["#celular_cli"] = "O ser é um campo obrigatório";
			} 
				
			
			//VALIDAR TELEFONE
	
			if(empty($data["telefone"])){
				$json["error_list"]["#telefone_cli"] = "O ser é um campo obrigatório";
			} 
			//VALIDAR ENDERECO
	
			if(empty($data["endereco"])){
				$json["error_list"]["#endereco_cli"] = "O ser é um campo obrigatório";
			} 
			
	
			
	
			//VALIDAR EMAIL
	
			if(empty($data["email"])){
				$json["error_list"]["#email"] = "O ser é um campo obrigatório";
			} 
			else{
			  if($this->users_model->is_duplicate("email", $data["email"]. $data["id_cliente"])){
				  $json["error_list"]["#email"] = "email já existe";
			  }else{
				if($data["email"]!=$data["mail_confirm"]){
					$json["error_list"]["#email"] = "";
					$json["error_list"]["#mail_confirm"] = "Emails divergentes";
				}
			  }
				
			}
	
	
			//VALIDAR SENHA
			if(empty($data["password"])){
				$json["error_list"]["#password_cli"] = "O ser é um campo obrigatório";
			} 
			else{
				if($data["password"]!=$data["password_confirm"]){
					$json["error_list"]["#password_cli"] = "";
					$json["error_list"]["#password_confirm"] = "Senhas divergentes";
				}
			  }
				
			
			//VALIDAR IMAGEM
	
			if(!empty($json["error_list"])){
				$json["status"] = 0;
			} 
			else{
			  //$data["password_hash"] = password_hash($data["password"].PASSWORD_DEFAULT);
			
			  
			  unset($data["password"]);
			  unset($data["password_confirm"]);
			  unset($data["mail_confirm"]);
			  unset($data["login"]);
				//VALIDAR ID
	
				if(empty($data["id_cliente"])){
					
					unset($data["id_cliente"]);
					$this->users_model->insert($data);
					

				}
				else{
					$id_cliente = $data["id_cliente"];
					unset($data["id_cliente"]);
					$this->users_model->update($id_cliente, $data);
				}
	
			}
			
			echo json_encode($json);
	
		}


		public function ajax_get_servico_data() {

			if (!$this->input->is_ajax_request()) {
				exit("Nenhum acesso de script direto permitido!");
			}
		
			$json = array();
			$json["status"] = 1;
			$json["input"] = array();
		
			$this->load->model("servicos_model");
		
			$id_servico = $this->input->post("id_servico");
			$data = $this->servicos_model->get_data($id_servico)->result_array()[0];
			$json["input"]["id_servico"] = $data["id_servico"];
			$json["input"]["nm_servico"] = $data["nm_servico"];
			$json["input"]["preco"] = $data["preco"];

			$json["img"]["servico_img_path"] = base_url() . $data["course_img"];

		
		
			echo json_encode($json);
		}

		public function ajax_get_prof_data() {

			if (!$this->input->is_ajax_request()) {
				exit("Nenhum acesso de script direto permitido!");
			}
	
			$json = array();
			$json["status"] = 1;
			$json["input"] = array();
	
			$this->load->model("profissionais_model");
	
			$id_profissional = $this->input->post("id_profissional");
			$data = $this->profissionais_model->get_data($id_profissional)->result_array()[0];
			$json["input"]["id_profissional"] = $data["id_profissional"];
			$json["input"]["nome"] = $data["nome"];
			$json["input"]["cpf_profissional"] = $data["cpf_profissional"];
			$json["input"]["celular"] = $data["celular"];
			$json["input"]["telefone"] = $data["telefone"];
			$json["input"]["endereco"] = $data["endereco"];
			$json["input"]["email_profissional"] = $data["email_profissional"];
			$json["input"]["conf_email"] = $data["email_profissional"];
			$json["img"]["prof_img_path"] = base_url() . $data["team_img"];
	
			echo json_encode($json);
		}


		public function ajax_get_cli_data() {

			if (!$this->input->is_ajax_request()) {
				exit("Nenhum acesso de script direto permitido!");
			}
	
			$json = array();
			$json["status"] = 1;
			$json["input"] = array();
	
			$this->load->model("users_model");
	
			$id_cliente = $this->input->post("id_cliente");
			$data = $this->users_model->get_data($id_cliente)->result_array()[0];
			$json["input"]["id_cliente"] = $data["id_cliente"];
			$json["input"]["nome_cli"] = $data["nome"];
			$json["input"]["cliente_cpf"] = $data["cliente_cpf"];
			$json["input"]["cli_celular"] = $data["celular"];
			$json["input"]["telefone_cli"] = $data["telefone"];
			$json["input"]["endereco_cli"] = $data["endereco"];
			$json["input"]["email"] = $data["email"];
			$json["input"]["mail_confirm"] = $data["email"];
			
	
			echo json_encode($json);
		}

		public function ajax_del_servico_data() {

			if (!$this->input->is_ajax_request()) {
				exit("Nenhum acesso de script direto permitido!");
			}
		
			$json = array();
			$json["status"] = 1;

		
			$this->load->model("servicos_model");		
			$id_servico = $this->input->post("id_servico");
			$this->servicos_model->delete($id_servico);
		
			echo json_encode($json);
		}



		public function ajax_del_prof_data() {

			if (!$this->input->is_ajax_request()) {
				exit("Nenhum acesso de script direto permitido!");
			}
		
			$json = array();
			$json["status"] = 1;

		
			$this->load->model("profissionais_model");		
			$id_profissional = $this->input->post("id_profissional");
			$this->profissionais_model->delete($id_profissional);
		
			echo json_encode($json);
		}
		
		
		public function ajax_del_cli_data() {

			if (!$this->input->is_ajax_request()) {
				exit("Nenhum acesso de script direto permitido!");
			}
		
			$json = array();
			$json["status"] = 1;

		
			$this->load->model("users_model");		
			$id_cliente = $this->input->post("id_cliente");
			$this->users_model->delete($id_cliente);
		
			echo json_encode($json);
		}

		

		public function ajax_list_servico()
		{
			if(!$this->input->is_ajax_request()){
				exit("Acesso direto não permitido");
			}

			$this->load->model("servicos_model");
			$servicos = $this->servicos_model->get_datatable();

			$data = array();
			foreach ($servicos as $servico){
				$row = array();
				$row[] = $servico->nm_servico;

				if($servico->course_img){
					$row []= '<img src="'.base_url() . $servico->course_img .'" style="max-height: 100px; max-width: 100px;">';

				}else{
					$row[]="";
				}
				$row[] = $servico->preco;

				$row[] = '<div style="display: inline-block;">
						<button class="btn btn-primary btn-edit-servico" 
							id_servico="'.$servico->id_servico.'">
							<i class="far fa-edit"></i>
						</button>
						<button class="btn btn-danger btn-del-servico" 
							id_servico="'.$servico->id_servico.'">
							<i class="far fa-trash-alt"></i>
						</button>
					</div>';

			$data[] = $row;

		}

		$json = array(
			"draw" => $this->input->post("draw"),
			"recordsTotal" => $this->servicos_model->records_total(),
			"recordsFiltered" => $this->servicos_model->records_filtered(),
			"data" => $data,
		);

		echo json_encode($json);
	}




	public function ajax_list_profissional()
	{
		if(!$this->input->is_ajax_request()){
			exit("Acesso direto não permitido");
		}

		$this->load->model("profissionais_model");
		$profissionais = $this->profissionais_model->get_datatable();

		$data = array();
		foreach ($profissionais as $profissional){
			$row = array();
		
			$row[] = $profissional->nome;			
			$row[] = $profissional->cpf_profissional;
			$row[] = $profissional->email_profissional;
			$row[] = $profissional->endereco;
			$row[] = $profissional->telefone;
			$row[] = $profissional->celular;
;

			if($profissional->team_img){
				$row []= '<img src="'.base_url() . $profissional->team_img .'" style="max-height: 100px; max-width: 100px;">';

			}else{
				$row[]="";
			}
			$row[] = '<div style="display: inline-block;">
					<button class="btn btn-primary btn-edit-profissional" 
						id_profissional="'.$profissional->id_profissional.'">
						<i class="far fa-edit"></i>
					</button>
					<button class="btn btn-danger btn-del-profissional" 
						id_profissional="'.$profissional->id_profissional.'">
						<i class="far fa-trash-alt"></i>
					</button>
				</div>';

		$data[] = $row;

	}

	$json = array(
		"draw" => $this->input->post("draw"),
		"recordsTotal" => $this->profissionais_model->records_total(),
		"recordsFiltered" => $this->profissionais_model->records_filtered(),
		"data" => $data,
	);

	echo json_encode($json);
}



	public function ajax_list_cliente()
	{
		if(!$this->input->is_ajax_request()){
			exit("Acesso direto não permitido");
		}

		$this->load->model("users_model");
		$clientes = $this->users_model->get_datatable();

		$data = array();
		foreach ($clientes as $cliente){
			$row = array();
		
			$row[] = $cliente->nome;			
			$row[] = $cliente->cliente_cpf;
			$row[] = $cliente->email;
			$row[] = $cliente->endereco;
			$row[] = $cliente->telefone;
			$row[] = $cliente->celular;
;

			$row[] = '<div style="display: inline-block;">
					<button class="btn btn-primary btn-edit-cliente" 
						id_cliente="'.$cliente->id_cliente.'">
						<i class="far fa-edit"></i>
					</button>
					<button class="btn btn-danger btn-del-cliente" 
						id_cliente="'.$cliente->id_cliente.'">
						<i class="far fa-trash-alt"></i>
					</button>
				</div>';

		$data[] = $row;

	}

	$json = array(
		"draw" => $this->input->post("draw"),
		"recordsTotal" => $this->users_model->records_total(),
		"recordsFiltered" => $this->users_model->records_filtered(),
		"data" => $data,
	);

	echo json_encode($json);
}



			
		
	






	/*//SALVAR E VALIDAR FORMULARIO PROFISSIONAIS
	public function ajax_teste_tp_login(){
 
		if(!$this->input->is_ajax_request()){
			exit("Acesso direto não permitido");
		}
		if($_SESSION['tp_login']==1){
			echo('<script>window.alert("HAHAHAHAHAH")</script>');
		}
		else{
  			echo('<script>window.alert("Sem permissão de acesso!")</script>');
		}*/
  






}
	

?>