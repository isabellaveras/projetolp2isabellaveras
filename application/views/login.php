<section style="min-height: calc(75vh - 83px)" class="light-bg">
	<div class="container"style="margin-top: 10%; margin-left: 25%;margin-right: 25%">
		<div class="row">
			<div class="col-lg-offset-3 col-lg-6 text-center">
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="section-title">
							<h2>LOGIN</h2>
						</div>
						<form id="login_form" method="post">
							
									<div class="form-group">
										<div class="input-group">
											<div class="input-group-addon">
												<span><i class="fas fa-user fa 1000px"></i></span>
											</div>
											<input type="text" placeholder="Usuário" id="username" name="username"
											class="form-control">
										</div>
										<span class="help-block"></span>
									</div>
							

							
									<div class="form-group">
										<div class="input-group">

											<div class="input-group-addon">
												<span ><i class="fas fa-lock"></i></span>
											</div>

											<input type="password" placeholder="Senha" name="password"
											class="form-control">
										</div>
									</div>
							
									<div class="form-group">
										<button type="submit" id="btn_login" class="btn btn-block">Login</button>
									</div>
									<span class="help-block"></span>
						</form> 
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- /.container -->
</section>

