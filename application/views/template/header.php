<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Material Design Bootstrap</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url(); ?>public/css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="<?php echo base_url(); ?>public/css/mdb.min.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="<?php echo base_url(); ?>public/css/style.min.css" rel="stylesheet">
</head>

<?php if (isset($styles)) {
			foreach ($styles as $style_name) {
				$href = base_url() . "public/css/" . $style_name; ?>
				<link href="<?=$href?>" rel="stylesheet">
			<?php }
		} ?>

<body>

  <!--Main Navigation-->
  <header>

    <!-- Navbar -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-light nav-shrink white scrolling-navbar">
      <div class="container">

        <!-- Brand -->
        <a class="navbar-brand waves-effect" href="<?php echo base_url(); ?>">
          <strong class="black-text">Imperial</strong>
        </a>

        <!-- Collapse -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Links -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">

          <!-- Left -->
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link waves-effect" href="<?php echo base_url(); ?>home">Principal
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link waves-effect" href="<?php echo base_url(); ?>sobre" >Sobre</a>
            </li>
            <li class="nav-item">
              <a class="nav-link waves-effect" href="<?php echo base_url(); ?>servicos">Serviços</a>
            </li>
            <li class="nav-item">
              <a class="nav-link waves-effect" href="<?php echo base_url(); ?>contato" >Contato</a>
            </li>
          </ul>

          <!-- Right -->
          <ul class="navbar-nav nav-flex-icons">
            <li class="nav-item">
              <a href="<?php echo base_url(); ?>restrict" class="nav-link border border-light rounded waves-effect"
                >
                <i class="fab fa-github mr-2"></i>Login
              </a>
            </li>
          </ul>

        </div>

      </div>
    </nav>
    <!-- Navbar -->

  </header>
