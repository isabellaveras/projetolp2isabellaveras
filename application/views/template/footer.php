<!--Footer-->
<footer class="page-footer text-center font-small mdb-color darken-2 mt-4 wow fadeIn">


<!--/.Call to action-->

<hr class="my-4">

<!-- Social icons -->
<div class="pb-4">
  <a href="https://www.facebook.com/" target="_blank">
    <i class="fab fa-facebook-f mr-3"></i>
  </a>

  <a href="https://www.google.com.br/" target="_blank">
    <i class="fab fa-google-plus-g mr-3"></i>
  </a>

  <a href="https://pinterest.com/" target="_blank">
    <i class="fab fa-pinterest mr-3"></i>
  </a>

  <a href="https://www.instagram.com/" target="_blank">
    <i class="fab fa-instagram mr-3"></i>
  </a>
</div>
<!-- Social icons -->

<!--Copyright-->
<div class="footer-copyright py-3">
  © 2019 Copyright:  Isabella Veras
</div>
<!--/.Copyright-->

</footer>
<!--/.Footer-->