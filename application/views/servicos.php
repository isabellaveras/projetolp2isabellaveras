<section id="servico" class="light-bg" style="margin-top: 100px;margin-left: 10%;margin-right: 10%;">
    <div class="text-center"><h1>SERVIÇOS</h1><br><br><br><br><hr> 

      <div class="row mb-4 wow fadeIn">
      

        <?php 
        
				if (!empty($_SESSION['serv'])) {

					foreach ($_SESSION['serv'] as $servico) { ?>

          <!--Grid column-->
          <div class="col-lg-4 col-md-6 mb-4">
          

            <!--Card-->
            <div class="card"style="height: 400px;">

              <!--Card image-->
              <div class="view overlay">
                <img src="<?=base_url().$servico["course_img"]?>" style="margin-top: 25px;" class="card-img-top"
                  alt="">                
                  <div class="mask rgba-white-slight"></div>
                </a>
              </div>

             
              <!--Card content-->
              <div class="card-body">
                <!--Title-->
                <h4 class="card-title"><?=$servico["nm_servico"]?></h4>
                <!--Text-->
                <p class="card-text">R$<?=$servico["preco"]?>,00</p>
                <a href="https://mdbootstrap.com/education/tech-marketing/automated-app-introduction/" target="_blank"
                  class="btn btn-primary btn-md">Agende já
                  <i class="fas fa-play ml-2"></i>
                </a>
              </div>

            </div>
            <!--/.Card-->

          </div>

        <?php } // FOREACH
        
				} // IF ?>

      </div>
    
      </div>
   
      

		</section>

 
    