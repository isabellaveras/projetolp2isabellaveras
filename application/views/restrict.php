<?php //mudar pafra funcao e javascript
/*if($_SESSION['tp_login']==1)
{
  echo('<script>window.alert("HAHAHAHAHAH")</script>');
}
else
{
  echo('<script>window.alert("Sem permissão de acesso!")</script>');
}*/
?>
<section style="min-height: calc(75vh - 83px)" class="light-bg">

	<div class="container"style="margin-top: 85px; ">
		<div class="row">
			<div class="col-lg-offset-3 col-lg-6 text-center">
				<div class="section-title" style="margin-left: 400px;width: 300px;">
						<h2>ÁREA RESTRITA</h2>
        </div>
			</div>
		</div>

  <div class="row">
    <div class="col-lg-offset-5 col-lg-2 text-center">
      <div class="form-group">
				<a class="btn-sm btn aqua-gradient " href="restrict/logoff" ><i class="fas fa-sign-out-alt"></i></a>
      </div>
    </div>
  </div>
  
  <div class="container">

    <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item"><a class="nav-link active" href="#tab_courses" role="tab" data-toggle="tab"> Serviços</a></li>
      <li class="nav-item"><a class="nav-link " href="#tab_team" role="tab" data-toggle="tab">Profissionais</a></li>
      <li class="nav-item"><a class="nav-link " href="#tab_user" role="tab" data-toggle="tab">Cliente</a></li>      
    </ul>

    <div class="tab-content">
      <div id="tab_courses" class="tab-pane active">Conteudo Serviços
        <div class="conatiner-fluid">
          <h2 class="text-center"><strong>Gerenciar Serviços</strong></h2>
          <a  id="btn_add_course" class="btn aqua-gradient "><i class="fas fa-plus">&nbsp;&nbsp;Adicionar Serviços</i></a>
          <table id="dt_couses" class="table table-striped table-bordered">
            <thead>
              <tr class="tableheader">
                <th class="dt-center">Nome</th>
                <th class="dt-center no-sort">Imagem</th>
                <th class="dt-center">Preço</th>
                <th class="dt-center no-sort">Ações</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
      <div id="tab_team" class="tab-pane ">Conteudo Profissionais
        <div class="conatiner-fluid">
            <h2 class="text-center"><strong>Gerenciar Profissionais</strong></h2>
            <a  id="btn_add_team" class="btn aqua-gradient "><i class="fas fa-plus">&nbsp;&nbsp;Adicionar Profissionais</i></a>
            <table id="dt_team" class="table table-striped table-bordered">
              <thead>
                <tr class="tableheader">                  
                  <th class="dt-center">Nome</th>
                  <th class="dt-center">CPF Profissional</th>
                  <th class="dt-center ">Email Profissional</th> 
                  <th class="dt-center no-sort">Endereço</th>
                  <th class="dt-center no-sort">Telefone</th>
                  <th class="dt-center no-sort">Celular</th>
                  <th class="dt-center">Foto</th>
                  <th class="dt-center no-sort">Ações</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
      </div>
      <div id="tab_user" class="tab-pane ">Conteudo Cliente
        <div class="conatiner-fluid">
            <h2 class="text-center"><strong>Gerenciar Clientes</strong></h2>
            <a  id="btn_add_user" class="btn aqua-gradient "><i class="fas fa-plus">&nbsp;&nbsp;Adicionar Cliente</i></a>
            <table id="dt_user" class="table table-striped table-bordered">
              <thead>
                <tr class="tableheader">
                  <th class="dt-center no-sort">Nome</th>
                  <th class="dt-center no-sort">CPF </th>
                  <th class="dt-center no-sort">Email </th>
                  <th class="dt-center">Endereço</th>
                  <th class="dt-center">Telefone</th>
                  <th class="dt-center">Celular</th>
                  <th class="dt-center">Ações</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
      </div>
  

    </div>
  </div>
</section>






<!--Modais-->


<!--SERVIÇO-->

<div id="modal_course" class="modal fade">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Serviço</h4>
        <button class="close" type="button" data-dismiss="modal">x</button>        
      </div>
      <div class="modal-body">
      <!--FORMULARIO-->
        <form id="form_course">          
          <input id="id_servico" name="id_servico" hidden>
          <!--Campo NOME-->
          <div class="form-group">
            <label class="col-lg-2 control-label">Nome</label>
            <div class="col-lg-10">
              <input id="nm_servico" name="nm_servico" class="form-control" maxlength="100">
              <span class="help-block"></span>
            </div>
          </div>
          <!--Campo IMAGEM-->
          <div class="form-group">

            <label class="col-lg-2 control-label">Imagem</label>
            <div class="col-lg-10">
              <img id="servico_img_path" src="" style="max-height:400px; max-height:400px;">
                <label class="btn ntn-block btn-info">
                <i class="fa fa-upload"></i>&nbsp;&nbsp;Importar Imagem
                <input type="file" id="btn_upload_servico_img" accept="*image/" style="display:none";>
                </label>
              <input id="course_img" name="course_img" hidden>
              <span class="help-block"></span>
            </div>
          </div>
          <!--Campo PROÇO-->
          <div class="form-group">
            <label step="0.1" class="col-lg-2 control-label">Preço</label>
            <div class="col-lg-10">
              <input type="float" min ="0"id="preco" name="preco" class="form-control" >
              <span class="help-block"></span>
            </div>
          </div>
          <!--Campo SALVAR-->
          <div class="form-group text-center">
            <button id="btn_save_course" id="btn btn-primary">
              <i class="fas fa-save"></i>Salvar
            </button>
            <span class="help-block"></span>
          </div>
        </form>
      </div> 
    </div>
  </div>
</div>


<!--Modal PROFISSIONAL-->
<div id="modal_team" class="modal fade">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Profissional</h4>
        <button class="close" type="button" data-dismiss="modal">x</button>
        
      </div>
        <div class="modal-body">
        <!--FORMULARIO-->
        <form id="form_profissional">
          <input id="id_profissional" name="id_profissional" hidden>
          <!--Campo LOGIN-->
          <div class="form-group">
            <label class="col-lg-2 control-label">Login</label>
            <div class="col-lg-10">
              <input id="login" name="login" class="form-control" maxlength="100">
              <span class="help-block"></span>
            </div>
          </div>
          <!--Campo NOME-->
          <div class="form-group">
            <label class="col-lg-2 control-label">Nome</label>
            <div class="col-lg-10">
              <input id="nome" name="nome" class="form-control" maxlength="100">
              <span class="help-block"></span>
            </div>
          </div>
         <!--Campo IMAGEM-->
         <label class="col-lg-2 control-label">Imagem</label>
            <div class="col-lg-10">
              <img id="prof_img_path" src="" style="max-height:400px; max-height:400px;">
                <label class="btn ntn-block btn-info">
                <i class="fa fa-upload"></i>&nbsp;&nbsp;Importar Imagem
                <input type="file" id="btn_upload_prof_img" accept="*image/" style="display:none";>
                </label>
              <input id="team_img" name="team_img"hidden>
              <span class="help-block"></span>
            </div>
          </div>

          <!--Campo CPF-->
          <div class="form-group">
            <label class="col-lg-2 control-label">CPF</label>
            <div class="col-lg-10">
              <input id="cpf_profissional" name="cpf_profissional" class="form-control" maxlength="11">
              <span class="help-block"></span>
            </div>
          </div>
          <!--Campo CELULAR-->
          <div class="form-group">
            <label class="col-lg-2 control-label">Celular</label>
            <div class="col-lg-10">
              <input id="celular" name="celular" class="form-control" maxlength="11">
              <span class="help-block"></span>
            </div>
          </div>
          <!--Campo TELEFONE-->
          <div class="form-group">
            <label class="col-lg-2 control-label">Telefone</label>
            <div class="col-lg-10">
              <input id="telefone" name="telefone" class="form-control" maxlength="10">
              <span class="help-block"></span>
            </div>
          </div>
          <!--Campo ENDERECO-->
          <div class="form-group">
            <label class="col-lg-2 control-label">Endereço</label>
            <div class="col-lg-10">
              <input id="endereco" name="endereco" class="form-control" maxlength="100">
              <span class="help-block"></span>
            </div>
          </div>

          <!--Campo EMAIL-->
          <div class="form-group">
            <label class="col-lg-2 control-label">E-Mail</label>
            <div class="col-lg-10">
              <input id="email_profissional" name="email_profissional" class="form-control" maxlength="100">
              <span class="help-block"></span>
            </div>
          </div>
          <!--Campo CONFIMA EMAIL-->
          <div class="form-group">
            <label class="col-lg-3 control-label">Confirmar E-Mail</label>
            <div class="col-lg-10">
              <input id="conf_email" name="conf_email" class="form-control" maxlength="100">
              <span class="help-block"></span>
            </div>
          </div>
          <!--Campo SENHA-->
          <div class="form-group">
            <label class="col-lg-2 control-label">Senha</label>
            <div class="col-lg-10">
              <input type="password" id="password" name="password" class="form-control" maxlength="100">
              <span class="help-block"></span>
            </div>
          </div>
          <!--Campo CONFIRMA SENHA-->
          <div class="form-group">
            <label class="col-lg-3 control-label">Confirmar Senha</label>
            <div class="col-lg-10">
              <input type="password" id="password_confirm" name="password_confirm" class="form-control" maxlength="100">
              <span class="help-block"></span>
            </div>
          </div>          
          <!--Campo SALVA-->
          <div class="form-group text-center">
            <button id="btn_save_team" id="btn btn-primary">
              <i class="fas fa-save"></i>&nbsp;&nbsp;Salvar
            </button>
            <span class="help-block"></span>
          </div>
          </form>
        </div>
    </div>
  </div>
</div>


<!--Modal CLIENTE-->
<div id="modal_user" class="modal fade">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Cliente</h4>
        <button class="close" type="button" data-dismiss="modal">x</button>        
      </div>
        <!--FORMULARIO-->
        <div class="modal-body">
          <form id="form_cliente">

           <input id="id_cliente" name="id_cliente" hidden>
            <!--Campo NOME-->
            <div class="form-group">
              <label class="col-lg-3 control-label">Nome Completo</label>
              <div class="col-lg-10">
                <input id="nome_cli" name="nome" class="form-control" maxlength="100">
                <span class="help-block"></span>
              </div>
            </div>
            <!--Campo LOGIN-->
            <div class="form-group">
              <label class="col-lg-2 control-label">Login</label>
              <div class="col-lg-10">
                <input id="login" name="login" class="form-control" maxlength="100">
                <span class="help-block"></span>
              </div>
            </div>
            <!--Campo CELULAR-->
            <div class="form-group">
              <label class="col-lg-2 control-label">Celular</label>
              <div class="col-lg-10">
                <input id="cli_celular" name="celular" class="form-control" >
                <span class="help-block"></span>
              </div>
            </div>
            <!--Campo CPF-->
            <div class="form-group">
              <label class="col-lg-2 control-label">CPF</label>
              <div class="col-lg-10">
                <input id="cliente_cpf" name="cliente_cpf" class="form-control" maxlength="11">
                <span class="help-block"></span>
              </div>
            </div>
            <!--Campo TELEFONE-->
            <div class="form-group">
              <label class="col-lg-2 control-label">Telefone</label>
              <div class="col-lg-10">
                <input id="telefone_cli" name="telefone" class="form-control" maxlength="10">
                <span class="help-block"></span>
              </div>
            </div>
            <!--Campo ENDERECO-->
            <div class="form-group">
              <label class="col-lg-2 control-label">Endereço</label>
              <div class="col-lg-10">
                <input id="endereco_cli" name="endereco" class="form-control" maxlength="100">
                <span class="help-block"></span>
              </div>
            </div>
            <!--Campo EMAIL-->
            <div class="form-group">
              <label class="col-lg-2 control-label">E-Mail</label>
              <div class="col-lg-10">
                <input id="email" name="email" class="form-control" maxlength="100">
                <span class="help-block"></span>
              </div>
            </div>
            <!--Campo CONFIMA EMAIL-->
            <div class="form-group">
              <label class="col-lg-3 control-label">Confirmar E-Mail</label>
              <div class="col-lg-10">
                <input id="mail_confirm" name="mail_confirm" class="form-control" maxlength="100">
                <span class="help-block"></span>
              </div>
            </div>
            <!--Campo SENHA-->
            <div class="form-group">
              <label class="col-lg-2 control-label">Senha</label>
              <div class="col-lg-10">
                <input type="password" id="password_cli" name="password" class="form-control" maxlength="100">
                <span class="help-block"></span>
              </div>
            </div>
            <!--Campo CONFIRMA SENHA-->
            <div class="form-group">
              <label class="col-lg-3 control-label">Confirmar Senha</label>
              <div class="col-lg-10">
                <input type="password" id="password_confirm" name="password_confirm" class="form-control" maxlength="100">
                <span class="help-block"></span>
              </div>
            </div>

            
            <!--Campo SALVAR-->
            <div class="form-group text-center">
              <button id="btn_save_user" id="btn btn-primary">
                <i class="fas fa-save"></i>&nbsp;&nbsp;Salvar
              </button>
              <span class="help-block"></span>
            </div>
          </form>
        </div>
    </div>
  </div>
</div>
