<section style="min-height: calc(100vh - 83px); padding: 100px;" class="light-bg">

    <h1 class="text-center">Sobre</h1><br><br>
    <p>Uma barbearia localizada na zona oeste de São Paulo na <strong> Rua Heitor Bastos Tigres n° 250</strong>, em funcionamento desde 2016, onde atualmente possui 3 funcionairos atendendo. Nas comodidades da barbearia você encontra TV, Wi-Fi, uma geladeira que possui deiversos tipos de cerveja.</p>

</section>

<section id="profissionais" class="light-bg" style="margin-left:10%;margin-right: 10%;">
    <div class="text-center"><h1>Profissionais</h1><br><br><br><br><hr> 

      <div class="row mb-4 wow fadeIn">
      

        <?php 
        
			if (!empty($_SESSION['prof'])) {
			    foreach ($_SESSION['prof'] as $profissional) { ?>

          <!--Grid column-->
          <div class="col-lg-4 col-md-6 mb-4">
          

            <!--Card-->
            <div class="card" style="height: 500px;">

              <!--Card image-->
              <div class="view overlay">
                <img src="<?=base_url().$profissional["team_img"]?>" class="card-img-top"
                  alt="">                
                  <div class="mask rgba-white-slight"></div>
                </a>
              </div>

             
              <!--Card content-->
              <div class="card-body">
                <!--Title-->
                <h4 class="card-title"><?=$profissional["nome"]?></h4>
                <!--Text-->
                <p class="card-text"><?=$profissional["celular"]?><br><?=$profissional["email_profissional"]?></p>
               
              </div>

            </div>
            <!--/.Card-->

          </div>

        <?php } // FOREACH
        
				}else{
                    echo("vazio");
                  } // IF ?>

      </div>
    
      </div>
   
      

</section>
