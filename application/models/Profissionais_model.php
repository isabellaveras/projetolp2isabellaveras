<?php

    class Profissionais_model extends CI_Model{

        public function __construct(){
            parent::__construct();
            $this->load->database();
        }

            //SELECT NA TABELA PROFISSIONAIS
        
        public function get_data($id, $select = NULL){
            if(!empty($select)){
                $this->db->select($select);
            }
            $this->db->from("profissional");
            $this->db->where("id_profissional", $id);
            return $this->db->get();
        }
            //INSERIR NA TABELA PROFISSIONAIS

        public function insert($data)
        {
            $profissional["cpf_profissional"]=$data["cpf_profissional"];
            $profissional["nome"]=$data["nome"];
            $profissional["endereco"]=$data["endereco"];
            $profissional["telefone"]=$data["telefone"];
            $profissional["celular"]=$data["celular"];
            $profissional["email_profissional"]=$data["email_profissional"];
            $profissional["team_img"]=$data["team_img"];
            $this->db->insert("profissional", $profissional);
            
        }

        //MOSTRAR O CONTEUDO DE SERVICOS
        public function show_profs(){

            $this->db->from("profissional");
            return $this->db->get()->result_array();
        }

            //UPDATE NA TABELA PROFISSIONAIS
        
        public function update($id, $data){
            
            $this->db->where("id_profissional", $id);
            $this->db->update("profissional", $data);
        }

            //DELETE NA TABELA PROFISSIONAIS

        public function delete($id){
            
            $this->db->where("id_profissional", $id);
            $this->db->delete("profissional");
        }

            //SE TIVER DUPLICIDADE

        public function is_duplicate($field, $value, $id = NULL){
            if(!empty($select)){
                $this->db->where("id_profissional <>", $id);
            }
            $this->db->from("profissional");
            $this->db->where($field, $value);
            return $this->db->get()->num_rows() >0;
        }




        
        var $column_search = array("nome", "email_profissional", "cpf_profissional");
        var $column_order = array("nome", "email_profissional", "cpf_profissional");
    
        private function _get_datatable() {
    
            $search = NULL;
            if ($this->input->post("search")) {
                $search = $this->input->post("search")["value"];
            }
            $order_column = NULL;
            $order_dir = NULL;
            $order = $this->input->post("order");
            if (isset($order)) {
                $order_column = $order[0]["column"];
                $order_dir = $order[0]["dir"];
            }
    
            $this->db->from("profissional");
            if (isset($search)) {
                $first = TRUE;
                foreach ($this->column_search as $field) {
                    if ($first) {
                        $this->db->group_start();
                        $this->db->like($field, $search);
                        $first = FALSE;
                    } else {
                        $this->db->or_like($field, $search);
                    }
                }
                if (!$first) {
                    $this->db->group_end();
                }
            }
    
            if (isset($order)) {
                $this->db->order_by($this->column_order[$order_column], $order_dir);
            }
        }
    
        public function get_datatable() {
    
            $length = $this->input->post("length");
            $start = $this->input->post("start");
            $this->_get_datatable();
            if (isset($length) && $length != -1) {
                $this->db->limit($length, $start);
            }
            return $this->db->get()->result();
        }
    
        public function records_filtered() {
    
            $this->_get_datatable();
            return $this->db->get()->num_rows();
    
        }
    
        public function records_total() {
    
            $this->db->from("profissional");
            return $this->db->count_all_results();
    
        }



    }
?>