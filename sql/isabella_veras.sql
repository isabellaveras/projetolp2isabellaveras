-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 06-Out-2019 às 22:21
-- Versão do servidor: 10.4.6-MariaDB
-- versão do PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `isabella_veras`
--
CREATE DATABASE IF NOT EXISTS `isabella_veras` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `isabella_veras`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--
-- Criação: 26-Set-2019 às 22:36
-- Última actualização: 06-Out-2019 às 20:08
--

CREATE TABLE `cliente` (
  `cliente_cpf` varchar(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `celular` varchar(11) NOT NULL,
  `telefone` varchar(10) DEFAULT NULL,
  `nome` varchar(100) NOT NULL,
  `idade` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='tabela de cadastro de cada cliente';

--
-- RELACIONAMENTOS PARA TABELAS `cliente`:
--

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`cliente_cpf`, `id_cliente`, `endereco`, `celular`, `telefone`, `nome`, `idade`, `email`) VALUES
('365412879', 21, 'rua da serra', '1196572354', '113652479', 'Leandro Robson', 25, 'robsonsouza@mail.com'),
('4475931455', 23, 'rua das palmeiras', '1196852474', '37726938', 'Joana Dark', 0, 'jonda@gmail.com'),
('44759314', 30, 'rua das palmeiras', '11981227264', '37726938', 'Laercio José souza', 0, 'leraciojose@mail.com');

-- --------------------------------------------------------

--
-- Estrutura da tabela `login`
--
-- Criação: 26-Set-2019 às 22:36
-- Última actualização: 06-Out-2019 às 19:30
--

CREATE TABLE `login` (
  `tp_login` int(11) NOT NULL COMMENT 'tipo login 1- restrito; 2- profissional; 3- cliente',
  `login` varchar(20) NOT NULL,
  `password_hash` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELACIONAMENTOS PARA TABELAS `login`:
--

--
-- Extraindo dados da tabela `login`
--

INSERT INTO `login` (`tp_login`, `login`, `password_hash`) VALUES
(1, 'administrador', '$2y$10$V7gjuW7LfYm8HuyRZCUuFeH5gu9tarNyLnQFXsdWw1y2rEAGVyvsu'),
(3, 'Cliente1', '$2y$10$XTPA3bA5oaSclhSCLvdZ0elyJXObiYVgav5dA3ft334ZljnNODGdm'),
(2, 'Profissional 1', '$2y$10$4qNF1vw6NK15ySzqUZEyieHRqDX9stFJBlrpOoZB88PyTAgWfAiBu');

-- --------------------------------------------------------

--
-- Estrutura da tabela `login_cliente`
--
-- Criação: 26-Set-2019 às 22:36
--

CREATE TABLE `login_cliente` (
  `id_cliente` int(11) NOT NULL,
  `login` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELACIONAMENTOS PARA TABELAS `login_cliente`:
--   `login`
--       `login` -> `login`
--   `id_cliente`
--       `cliente` -> `id_cliente`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `login_prof`
--
-- Criação: 26-Set-2019 às 22:36
--

CREATE TABLE `login_prof` (
  `id_profissional` int(11) NOT NULL,
  `login` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELACIONAMENTOS PARA TABELAS `login_prof`:
--   `login`
--       `login` -> `login`
--   `id_profissional`
--       `profissional` -> `id_profissional`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `profissional`
--
-- Criação: 28-Set-2019 às 20:08
-- Última actualização: 06-Out-2019 às 19:58
--

CREATE TABLE `profissional` (
  `id_profissional` int(11) NOT NULL,
  `cpf_profissional` varchar(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `telefone` varchar(10) NOT NULL,
  `celular` varchar(11) NOT NULL,
  `email_profissional` varchar(255) NOT NULL,
  `team_img` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELACIONAMENTOS PARA TABELAS `profissional`:
--

--
-- Extraindo dados da tabela `profissional`
--

INSERT INTO `profissional` (`id_profissional`, `cpf_profissional`, `nome`, `endereco`, `telefone`, `celular`, `email_profissional`, `team_img`) VALUES
(28, '63254198', 'Luana Teixeira', 'Paraisopolis', '1136524788', '119365247', 'luzinha@gmail.com', '/assets/img/profissionais/randon3.jpg'),
(29, '54123697', 'Ana Paula ', 'rua das palmeiras', '37726938', '11981227264', 'anapaula@mail.com', '/assets/img/profissionais/random1.jpg'),
(31, '6325514799', 'Leandro Robson', 'rua das palmeiras', '37726938', '11981227264', 'robson@outlook.com', '/assets/img/profissionais/randon2.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `servico`
--
-- Criação: 26-Set-2019 às 22:36
-- Última actualização: 06-Out-2019 às 18:09
--

CREATE TABLE `servico` (
  `nm_servico` varchar(50) NOT NULL,
  `id_servico` int(11) NOT NULL,
  `preco` float NOT NULL,
  `course_img` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELACIONAMENTOS PARA TABELAS `servico`:
--

--
-- Extraindo dados da tabela `servico`
--

INSERT INTO `servico` (`nm_servico`, `id_servico`, `preco`, `course_img`) VALUES
('Barba', 12, 45, '/assets/img/servicos/barba.png'),
('Cortes', 14, 20, '/assets/img/servicos/raspar1.png'),
('Raspar', 15, 30, '/assets/img/servicos/raspar.png'),
('Bigode', 16, 30, '/assets/img/servicos/migode.png');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id_cliente`),
  ADD UNIQUE KEY `CPF` (`cliente_cpf`);

--
-- Índices para tabela `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`login`);

--
-- Índices para tabela `login_cliente`
--
ALTER TABLE `login_cliente`
  ADD PRIMARY KEY (`id_cliente`,`login`),
  ADD KEY `login` (`login`);

--
-- Índices para tabela `login_prof`
--
ALTER TABLE `login_prof`
  ADD KEY `id_profissional` (`id_profissional`),
  ADD KEY `login` (`login`);

--
-- Índices para tabela `profissional`
--
ALTER TABLE `profissional`
  ADD PRIMARY KEY (`id_profissional`);

--
-- Índices para tabela `servico`
--
ALTER TABLE `servico`
  ADD PRIMARY KEY (`id_servico`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de tabela `profissional`
--
ALTER TABLE `profissional`
  MODIFY `id_profissional` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT de tabela `servico`
--
ALTER TABLE `servico`
  MODIFY `id_servico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `login_cliente`
--
ALTER TABLE `login_cliente`
  ADD CONSTRAINT `login_cliente_ibfk_1` FOREIGN KEY (`login`) REFERENCES `login` (`login`),
  ADD CONSTRAINT `login_cliente_ibfk_2` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id_cliente`);

--
-- Limitadores para a tabela `login_prof`
--
ALTER TABLE `login_prof`
  ADD CONSTRAINT `login_prof_ibfk_1` FOREIGN KEY (`login`) REFERENCES `login` (`login`),
  ADD CONSTRAINT `login_prof_ibfk_2` FOREIGN KEY (`id_profissional`) REFERENCES `profissional` (`id_profissional`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
